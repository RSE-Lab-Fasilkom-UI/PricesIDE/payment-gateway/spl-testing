# Postman Collection and Environment For Testing PaymentGateway Product Line

This repository contains a Postman collection and environment for testing API endpoints for 5 payment gateway product.

## Contents

- `PaymentGateway Product.postman_collection.json`: Postman Collection file
- `VMJPaymentGateway.postman_environment`: Postman environment file

## Prerequisites

- [Postman](https://www.postman.com/downloads/) installed on your machine.

## Importing the Collection and Environment

### Importing the Collection

1. Open Postman.
2. Click on the `Collections` tab in the left sidebar.
3. Click the `Import` button.
4. Select the `File` tab.
5. Click `Choose Files` and select `MyPostmanCollection.json` from this repository.
6. Click the `Import` button.

### Importing the Environment

1. Open Postman.
2. Click on the `Environments` tab in the left sidebar.
3. Click the `Import` button.
4. Select the `File` tab.
5. Click `Choose Files` and select `MyPostmanEnvironment.json` from this repository.
6. Click the `Import` button.

## Using the Collection

1. Select the imported environment from the environment dropdown in the top right corner of Postman.
2. Expand the imported collection in the `Collections` tab.
3. Select any request within the collection.
4. Click the `Send` button to execute the request.

## Notes

- Ensure the environment variables are correctly set before sending requests.
- You can edit the environment variables by clicking the eye icon next to the environment dropdown and then clicking `Edit`.